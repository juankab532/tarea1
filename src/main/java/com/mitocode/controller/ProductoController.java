
package com.mitocode.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Persona;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;


@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private IProductoService service;
	
	@GetMapping
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> lista = service.listar();
		return new ResponseEntity<List<Producto>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Producto> listarPorId(@PathVariable("id") Integer id) {
		//Producto obj = service.listarPorId(id);
		//return new ResponseEntity<Producto>(obj, HttpStatus.OK);
		System.out.println("entro");
		Producto obj = service.listarPorId(id);
		if(obj.getIdProducto()==null) {
			throw new ModeloNotFoundException("Id de producto no encontrado:"+id);
		}
		return new ResponseEntity<Producto>(obj, HttpStatus.OK); 
	}

	@GetMapping("/hateoas/{id}")
	public EntityModel<Producto> listarPorIdHateoas(@PathVariable("id") Integer id) {
		Producto obj = service.listarPorId(id);

		if(obj.getIdProducto()==null) {
			throw new ModeloNotFoundException("Id de producto no encontrado:"+id);
		}
		
		EntityModel<Producto> recurso= new EntityModel<Producto>(obj);
		WebMvcLinkBuilder linkTo= linkTo( methodOn( this.getClass() ).listarPorId(id) );
		recurso.add(linkTo.withRel("paciente-resource"));
		
		return recurso;
	}		
	
	@PostMapping
	public ResponseEntity<Producto> registrar(@Valid @RequestBody Producto objeto) {
		//Producto obj = service.registrar(objeto);
		//return new ResponseEntity<Producto>(obj, HttpStatus.CREATED);
		
		Producto obj = service.registrar(objeto);
		URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdProducto()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Producto> modificar(@Valid @RequestBody Producto objeto) {
		Producto obj = service.registrar(objeto);
		URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdProducto()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
}
