package com.mitocode.exception;

import java.time.LocalDateTime;

public class ExceptionResponse {
	private LocalDateTime fecha;
	private  String mensaje;
	private String detalles;
	
	public ExceptionResponse(LocalDateTime fecha, String mensaje, String detalles) {
		this.fecha = fecha;
		this.mensaje = mensaje;
		this.detalles = detalles;
	}
	public LocalDateTime getFechaHora() {
		return fecha;
	}
	public void setFecha(LocalDateTime fechaHora) {
		this.fecha = fechaHora;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getDetalles() {
		return detalles;
	}
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	
	
}
